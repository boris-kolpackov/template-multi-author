# template-multi-author

License/Copyright setup template for a project with multiple, changing
authors. For example, a project that accepts external constibutions.

1. Copyright notice(s) are in `LICENSE` if possible/desirable (e.g., MIT) or
   in `COPYRIGHT` otherwise.

2. The copyright notice (whether in `LICENSE` or in `COPYRIGHT`) should be a
   single line in the form:

   ```
   Copyright (c) <year> the <project> authors (see the AUTHORS file).
   ```

   This is important to prevent license mis-detection by GitHub, etc.

3. See the `AUTHORS` file for the suggested content (replace `<project>`).

4. See the `CONTRIBUTORS.md` file for the suggested content.
